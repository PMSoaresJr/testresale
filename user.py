import athena
from schema import Schema
from functools import wraps
import json
import boto3
import os
import uuid

from schema import Schema

s3 = boto3.client('s3')

BUCKET = os.getenv('BUCKET')
ATHENA_BUCKET = os.getenv('ATHENA_BUCKET')

class S3Model(object):
ma(dict)
    name = 'raw'

    @classmethod
    def validate(cls, obj):
        assert cls.SCHEMA._schema == dict or type(cls.SCHEMA._schema) == dict
        return cls.SCHEMA.validate(obj)

    @classmethod
    def save(cls, obj):
        object_id = obj.setdefault('id', str(uuid.uuid4()))
        obj = cls.validate(obj)
        s3.put_object(
            Bucket=BUCKET,
            Key=f'{cls.name}/{object_id}',
            Body=json.dumps(obj),
        )
        return obj

    @classmethod
    def load(cls, object_id):
        obj = s3.get_object(
            Bucket=BUCKET,
            Key=f'{cls.name}/{object_id}',
        )
        obj = json.loads(obj['Body'].read())
        return cls.validate(obj)

    @classmethod
    def delete_obj(cls, object_id):
        s3.delete_object(
            Bucket=BUCKET,
            Key=f'{cls.name}/{object_id}',
        )
        return {'deleted_id': object_id}

    @classmethod
    def list_ids(cls):
        bucket_content = s3.list_objects_v2(Bucket=BUCKET)

        object_ids = [
            file_path['Key'].lstrip(f'{cls.name}/')
            for file_path in bucket_content.get('Contents', [])
            if file_path['Size'] > 0
        ]

        return object_ids


def handle_api_error(func):

    @wraps(func)
    def wrapped_func(*args, **kwargs):
        try:
            return {
                'statusCode': 200,
                'body': json.dumps(func(*args, **kwargs)),
            }
        except Exception as e:
            return {
                'statusCode': 500,
                'body': str(e),
            }
    return wrapped_func


class S3ApiRaw(object):

    s3_model_cls = S3Model

    @classmethod
    @handle_api_error
    def get(cls, event, context):
        obj_id = event['pathParameters']['id']
        return cls.s3_model_cls.load(obj_id)

    @classmethod
    @handle_api_error
    def put(cls, event, context):
        obj_id = event['pathParameters']['id']
        obj = cls.s3_model_cls.load(obj_id)

        updates = json.loads(event['body'])
        obj.update(updates)

        return cls.s3_model_cls.save(obj)

    @classmethod
    @handle_api_error
    def post(cls, event, context):
        obj = json.loads(event['body'])
        if 'id' in obj:
            raise Exception('Do not specify id in resource creation')
        return cls.s3_model_cls.save(obj)

    @classmethod
    @handle_api_error
    def delete(cls, event, context):
        obj_id = event['pathParameters']['id']
        return cls.s3_model_cls.delete_obj(obj_id)

    @classmethod
    @handle_api_error
    def all(cls, event, context):
        return [cls.s3_model_cls.load(obj_id) for obj_id in cls.s3_model_cls.list_ids()]

    @classmethod
    def get_api_methods(self):
        return self.get, self.post, self.put, self.delete, self.all

class User(S3Model):
    """
        User:
        - name: "users"
        - schema:
           id: string
           nome: str,
           endereco: str,
           descricao: str,
           status: str,
           caracteristicas: str,
           tipo: str,
           finalidade: str,
           imobiliaria:str
    """
    name = 'user'
    SCHEMA = Schema({
           'id': string
           'nome': str,
           'endereco': str,
           'descricao': str,
           'status': str,
           'caracteristicas': str,
           'tipo': str,
           'finalidade': str,
           'imobiliaria':str
    })


class UserResource(S3ApiRaw):
    s3_model_cls = User

get, post, put, delete, _ = UserResource.get_api_methods()

@handle_api_error
def all(event, context):
    users = athena.get_results(f"""
        SELECT * FROM {athena.DB}.users
    """)
    return users